import React, { useRef, useState } from "react";
import CTAbutton from '../components/CTAbutton'
import {object, string} from 'yup'

function Login(props){

    const[errors, setErrors] = useState('')
    const[emailError,setEmailError] = useState(false)
    const[passwordError,setPasswordError] = useState(false)

    const loginForm = useRef(null)

    const loginSchema = object({
       email: string().required().email(),
       password: string().required()
    })

    async function handleLogin(e){
            e.preventDefault()
            const form = loginForm.current
            const email = form['email'].value
            const password = form['password'].value

            try{
                const userDetails = await loginSchema.validate({email,password});
                console.log(userDetails)  
            }
             catch(error){
                setEmailError(false)
                setPasswordError(false)
                setErrors(error.errors.join('.'))

               switch(error.path){
                    case "email":
                        setEmailError(true)
                        break;
                    case "password":
                        setPasswordError(true)
                        break;

                }
            }
             
    }

    return(
        <main className="container mx-auto py-12 px-4">
        <section>
            <form ref={loginForm} className="flex flex-col" onSubmit={handleLogin}>
                <label htmlFor="email">Email</label>
                <input type="email" id="email" className={`p-2 mt-2 mb-6 border border-violet-600 ${emailError?'border-red-600 border-2':'border-violet-600'}`}/>
                <label htmlFor="password">Password</label>
                <input type="password" id="password" className={`p-2 mt-2 mb-6 border border-violet-600 ${passwordError?'border-red-600 border-2':'border-violet-600'}`}/>
                <CTAbutton type="submit" action={()=>{}} text="Login"/>
            </form>
           <div className="flex flex-row justify-center mt-6"><span className="text-sm text-red-700">{errors}</span></div> 
        </section>
        </main>
    );
}

export default Login;