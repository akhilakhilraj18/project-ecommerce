import { useEffect, useState } from 'react'
import viteLogo from '/vite.svg'
import Product from '../components/Product'
import Header from '../components/Header'


function Home() {
  const[products, setproducts] = useState([])

 useEffect(()=>{
 fetch('https://fakestoreapi.com/products/')
            .then(res=>res.json())
            .then(json=>setproducts(json))
 }, [])

  return (
    
     
     <main className='container mx-auto py-12 px-4'>
      <section>
        <h2 className='text-2xl font-bold'>Reccomended Products</h2>
        <ul className='mt-6 grid gap-6 xl:gap-12 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 2xl:grid-cols-5'>
          {
            products.map(product=>{
              console.log(product)
              return(
               <Product key={product.id} product={product}/>
              )
            })
          }
          
        </ul>
      </section>
     </main>
     
    
  )
}

export default Home;
