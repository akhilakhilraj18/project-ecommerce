import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'

import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";


import store from './app/store.js'
import { Provider } from 'react-redux'
;
import Home from './routes/Home.jsx';
import Root from './routes/Root.jsx';
import Login from './routes/Login.jsx';

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root/>,
    children: [
        {
          path: "/",
          element: <Home/>,
        },
        {
          path: "/login",
          element: <Login/>
        }
    ]
  },
]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
  <Provider store={store}>
  <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>,
)
