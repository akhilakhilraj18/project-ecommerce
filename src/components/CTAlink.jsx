import React from "react";
import {Link} from 'react-router-dom'

function CTAlink(props){
    return(
       < Link to={props.link} className="inline-block flex flex-row justify-center items-center w-full bg-purple-700 hover:bg-purple-600 text-white h-10 rounded-sm px-4 ">{props.text}</Link>
    );
}
export default CTAlink;