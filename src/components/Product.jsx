import React from "react";
import { useDispatch } from "react-redux";
import { addToCart } from "../features/cart/cartSlice";
import CTAbutton from "./CTAbutton";



function Product(props) {
    const dispatch = useDispatch()
    const product = props.product
    return(
        <li className="flex flex-col justify-between">
            <div>
           <img className='w-full aspect-square object-contain object-center' src={product.image}alt=''></img>
            <h3>{product.title}</h3>
            <span>{product.price}$</span>
            </div>
            <CTAbutton action={()=>{dispatch(addToCart({product: product,quantity:1}))}} text="Add To Cart"/>
            
          </li>
    );
}

export default Product;