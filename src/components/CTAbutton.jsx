import React from "react";
function CTAbutton(props) {
    return(
       
         <button type={props.type?props.type: ''} onClick={props.action} className="w-full bg-purple-700 hover:bg-purple-600 text-white h-10 rounded-sm px-4 ">{props.text}</button>
       
    );
}

export default CTAbutton;