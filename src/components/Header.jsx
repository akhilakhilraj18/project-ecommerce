import React from "react";
import {useSelector} from 'react-redux';
import CTAlink from "./CTAlink";


function Header(props) {
  const cartItems = useSelector(state=>state.cart.cartItems)
  
    return(
    
        <header className='h-20 p-4 flex flex-row justify-between items-center shadow-lg'>
      <span>Urbanik</span>
      <nav className='hidden xl:block'>
        <ul className='flex flex-row gap-x-6'>
          <li>
            <a href='#'>Home</a>
          </li>
          <li>
            <a href='#'>About</a>
          </li>
          <li>
            <a href='#'>Products</a>
          </li>
          <li>
            <a href='#'>Contacts</a>
          </li>
        </ul>
        </nav>
        <div className="flex flex-row items-center gap-6">
        {/*<a href="#">
        <img className="w-6 h-6" src="/icons/user.svg" alt="" />
    </a>*/}
    <CTAlink text="Login" link="login"/>
      <a href='#'>
        <div className='flex flex-row'>
          <img src='/icons/cart.svg' alt=''/>
          <span className='font-bold text-xs text-red-500'>{cartItems.length}</span>
        </div>
      </a>
      <button className='xl:hidden'>
      <img className='w-6 h-6' src='/icons/menu.svg'alt='hamburger icon'></img>
      </button>
        </div>
     
      </header>
      
        
    );
}

export default Header;